## ORID

### O：

   1.We carried out the code review of yesterday's refactor operation this morning, and found that the reconstruction I did still had smell, which could be further optimized. 

   2.Then, together with the team members, we drew a concept map of the TDD content we learned before.

3. Learn the basic knowledge about http, including request path composition, status code meaning, request mode and so on.

4. Learned pair programming and implemented it in later exercises.

5. I learned the basic knowledge of spring boot, and implemented different request interfaces and verified them with postman.

### R:

fulfilling and intersting.

### I：

Through pair programming, I learned the way of thinking of team members and promoted our relationship. The drawing of concept map today allows me to have a summative review of what I have learned before. 

### D:

In the future learning review process, we will also use concept maps.