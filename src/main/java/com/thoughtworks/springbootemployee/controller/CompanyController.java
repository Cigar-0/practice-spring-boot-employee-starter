package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final EmployeeRepository employeeRepository = new EmployeeRepository();
    private final CompanyRepository companyRepository = new CompanyRepository(employeeRepository);
    @GetMapping
    public List<Company> companyList(){
        return companyRepository.getAllCompany();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.save(company);
    }
    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id){
        return companyRepository.getCompanyById(id);
    }
    @PutMapping("/{id}")
    public Company updateCompanyById(@PathVariable Long id, @RequestBody Company name){
        return companyRepository.updateCompanyById(id, name);
    }
    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return companyRepository.getEmployeesByCompanyId(id);
    }
    @DeleteMapping("/{id}")
    public void deleteCompanyById(@PathVariable Long id){
        companyRepository.deleteCompanyById(id);
    }
    @GetMapping(params = {"page", "size"})
    public List<Company> listCompany(@RequestParam Integer page, @RequestParam Integer size) {
        return companyRepository.listCompany(page, size);
    }



}
