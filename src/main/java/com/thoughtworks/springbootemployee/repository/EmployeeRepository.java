package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.controller.Employee;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private static final List<Employee> employees = new ArrayList<>();
    public Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }
    public Employee save(Employee employee){
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }
    public List<Employee> getAllEmployees() {
        return employees;
    }
    public Employee getEmployee(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateEmployeeById(Long id, Employee employee) {
        Employee updatedEmployee = getEmployee(id);
        if (Objects.nonNull(employee.getSalary())) {
            updatedEmployee.setSalary(employee.getSalary());
        }
        if (Objects.nonNull(employee.getAge())) {
            updatedEmployee.setAge(employee.getAge());
        }
        if (Objects.nonNull(employee.getCompanyId())) {
            updatedEmployee.setCompanyId(employee.getCompanyId());
        }
        return updatedEmployee;
    }

    public void delete(Long id) {
        employees.remove(getEmployee(id));
    }

    public List<Employee> listEmployees(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeeByCompanyId(Long id) {
        return employees.stream()
                .filter(employee -> id.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }

    public void clear() {
        employees.clear();
    }
}
