package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.controller.Company;
import com.thoughtworks.springbootemployee.controller.Employee;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CompanyRepository {
    private final List<Company> companyList = new ArrayList<>();
    private final EmployeeRepository employeeRepository;

    public CompanyRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    private Long generateId() {
        return companyList.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Company> getAllCompany() {
        return this.companyList;
    }

    public Company save(Company company) {
        company.setId(generateId());
        companyList.add(company);
        return company;
    }

    public Company getCompanyById(Long id) {
        return companyList.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public Company getCompany(Long id) {
        return companyList.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public Company updateCompanyById(Long id, Company company) {
        Company updateCompany = getCompany(id);
        if (Objects.nonNull(updateCompany.getName())) {
            updateCompany.setName(company.getName());
        }
        return updateCompany;
    }

    public void deleteCompanyById(Long id) {
        this.companyList.remove(getCompanyById(id));
    }


    public List<Company> listCompany(Integer page, Integer size) {
        return companyList.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(Long id) {
        return employeeRepository.getEmployeeByCompanyId(id);
    }
}
