package com.thoughtworks.springbootemployee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.controller.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    MockMvc client;
    @BeforeEach
    void setUp(){
        employeeRepository.clear();

    }
    @Test
    void should_create_employee_when_create_given_employee() throws Exception {
     //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        String employeeJson = new ObjectMapper().writeValueAsString(employee);
     //when

     //then
        client.perform(MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON).content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(13))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(4000));
    }
    @Test
    void should_get_employee_when_get_given_employeeId() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        employeeRepository.save(employee);
        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(13))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(4000));
    }
    @Test
    void should_get_all_employees_when_get_given_null() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        employeeRepository.save(employee);
        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)));
    }
    @Test
    void should_update_employees_when_unpdate_given_employee_and_id() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);

        Employee newEmployee =new Employee(1L, "Lily", 18, "female", 5000);
        String employeeJson = new ObjectMapper().writeValueAsString(newEmployee);
        employeeRepository.save(employee);
        //when

        //then
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", employee.getId())
                        .contentType(MediaType.APPLICATION_JSON).content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lily"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(18))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));;
    }
    @Test
    void should_delete_employee_when_delete_given_employeeId() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        Employee employee1 = new Employee(2L, "Lucy", 13, "female", 4000);

        employeeRepository.save(employee);
        employeeRepository.save(employee1);
        //when

        //then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", employee.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
        assertEquals(1, employeeRepository.getAllEmployees().size());
    }
    @Test
    void should_search_employees_when_get_given_gender() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        Employee employee1 = new Employee(2L, "Jerry", 13, "male", 4000);

        employeeRepository.save(employee);
        employeeRepository.save(employee1);
        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees?gender={gender}", employee.getGender()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].gender").value("female"));
    }
    @Test
    void should_search_employees_page_when_get_given_page_and_size() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lily", 13, "female", 4000);
        Employee employee1 = new Employee(2L, "Jerry", 13, "male", 4000);
        Employee employee2 = new Employee(3L, "Jerry", 13, "male", 4000);
        Employee employee3 = new Employee(3L, "Jerry", 13, "male", 4000);
        Employee employee4 = new Employee(2L, "Jerry", 13, "male", 4000);
        Employee employee5 = new Employee(2L, "Jerry", 13, "male", 4000);
        Employee employee6 = new Employee(2L, "Jerry", 13, "male", 4000);

        employeeRepository.save(employee);
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
        employeeRepository.save(employee3);
        employeeRepository.save(employee4);
        employeeRepository.save(employee5);
        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees?page={page}&size={size}", 1,5))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(5)));
    }

}
